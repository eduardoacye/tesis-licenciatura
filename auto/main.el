(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt" "letterpaper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "top=1.25in" "bottom=1.25in" "left=1.25in" "right=1.25in") ("inputenc" "utf8x") ("babel" "spanish") ("appendix" "title" "titletoc" "toc") ("mathpazo" "sc")))
   (TeX-run-style-hooks
    "latex2e"
    "chapters/contexto_historico"
    "chapters/introduccion"
    "chapters/cl_sin_tipos"
    "chapters/exploraciones"
    "chapters/teoria"
    "apendices/B_interprete"
    "article"
    "art12"
    "setspace"
    "geometry"
    "fancyhdr"
    "inputenc"
    "babel"
    "amsmath"
    "amsfonts"
    "amssymb"
    "bm"
    "fancybox"
    "backnaur"
    "listings"
    "appendix"
    "tikz"
    "sectsty"
    "natbib"
    "mathpazo")
   (TeX-add-symbols
    '("lab" 2)
    '("lap" 2)
    '("rom" 1)
    "calam"
    "acon"
    "aeqi"
    "bcon"
    "bred"
    "beqi"
    "ered"
    "eq"
    "aeq"
    "beq"
    "eeq"
    "ceq"
    "tra"
    "aco"
    "bre"
    "ere")
   (LaTeX-add-bibliographies
    "bibliography/biblio")))

