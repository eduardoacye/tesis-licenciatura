(TeX-add-style-hook
 "biblio"
 (lambda ()
   (LaTeX-add-bibitems
    "LCThenAndNow"
    "HistoryOfLC"
    "LCSyntaxSemantics")))

