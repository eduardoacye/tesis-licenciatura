% -*- coding: utf-8; mode: latex; mode: flyspell; ispell-local-dictionary: "castellano" -*-

\part{Introducción al cálculo $\lambda$}
% \shadowbox{%
%   \begin{minipage}{\dimexpr\textwidth-\shadowsize-2\fboxrule-2\fboxsep}
%     El objetivo de este capítulo será introducir los conceptos del \calam,
%     el lector deberá entender su importancia y utilidad.

%     Puntos importantes

%     \begin{itemize}
%     \item[\S] ¿Qué hace al \calam especial?
%     \item[\S] Presentar problemas y definiciones como juegos o acertijos.
%     \item[\S] Mantener prosa clara sin delimitar las ideas por secciones
%       concretas.
%     \item[\S] Desarrollar las ideas de tal manera que el flujo de
%       conceptos no se interrumpa.
%     \item[\S] Comparar y usar analogías con otras disciplinas mas
%       populares de las matemáticas y la computación.
%     \end{itemize}
%   \end{minipage}}\\

%\section{Idea intuitiva}
El \calam es un sistema matemático el cuál permite expresar, manipular
y estudiar funciones. La manera en como se trabaja con funciones en
este sistema es un poco diferente a como es usual en otras áreas de
las matemáticas.\\

En general se considera que una función es una regla de
correspondencia en donde cuando algúna cosa es dada (como argumento)
otra cosa (el valor de la función para ese argumento) es obtenido;
podemos concebir las funciones como programas que tienen una entrada
y una salida, al ejecutar el programa con un determinado valor de
entrada, éste siempre da como resultado un determinado valor de
salida.\\

Una función no está limitada a operar con argumentos arbitrarios, para
una función en particular se pueden identificar clases o rangos de
posibles argumentos y valores de la función. No es necesario
especificar la clase de argumentos y la clase de valores de una
función ya que podemos tratar la aplicación de una función a algo
fuera de la clase de argumentos asociada a ella como un valor sin
significado.\\

Por ejemplo, la función $f(x) = 3\times x$ tiene como rango de
argumentos y rango de valores a números. Al aplicar la función $f$ a
un número en particular como el $2$ se obtiene otro número, en este
caso $f(2) = 3\times 2 = 6$. Los valores de $f$ pudieran o no perder
sentido si se aplica $f$ a otra clase de argumentos: En el caso de
considerar las palabras en español como argumentos de $f$, no tienen
sentido los valores que puede tomar $f$ debido a que no hay una
definición precisa de multiplicación que involucre a palabras en el
español y números, por otro lado pudieramos ampliar la clase de
argumentos de $f$ a que incluya matrices, debido a que si hay una
operación definida de multiplicación entre un número (en este caso el
$3$) y una matriz.\\

Es posible considerar funciones cuya clase de argumentos sean
funciones, por ejemplo en el cálculo diferencial la derivada es una
función cuyas clases de argumentos y valores son a su vez funciones.
En general existen funciones que siempre tienen sentido sin importar
en que argumentos sea aplicada, una de estas funciones es la función
identidad $I$ la cual es definida como $I(x) = x$.

\section{Notación}

La notación utilizada previamente para escribir la definición y
aplicación de funciones en los párrafos de arriba es ampliamente usada
en matemáticas, sin embargo el \calam favorece una notación
alternativa.\\

Retomando un ejemplo previo, la expresión para la
definición de la función $f$ es $f(x)=3\times x$ y se compone de tres
elementos: primero se encuentra el nombre de la función seguido de una
letra que representa una variable, el símbolo $=$ establece una
relación del nombre de la función y el argumento con
otra expresión en donde puede o no aparecer la variable $x$, en este
caso esta otra expresión es $3\times x$ la cual representa una
multiplicación. En el \calam no se especifica el nombre de la función,
considerando una definición como anónima. La manera en como se
escribiría la definición de $f$ es $\lab{x}{3\times x}$, el nombre de
la función es omitido, la variable se encuentra después de el símbolo
$\lambda$ y lo que antes era el lado derecho de la función se
encuentra después del punto.\\

Debido a que en el \calam el nombre de la función no se encuentra en
su definición la aplicación de una función se escribe especificando
textualmente la función a la que se hace referencia. La manera en
como de denota la aplicación de $f$ en el número $2$ es:

$$\lap{\lab{x}{3\times x}}{2} \text{ en lugar de } f(2)$$\

A pesar de aparentar ser una notación mas inconveniente debido a que
se requiere escribir mas texto para expresar la misma idea, esta notación
nos permite ser mas explícitos en las operaciones que se realizan con
las funciones y proveen de una mayor uniformidad en la sintáxis del
lenguaje utilizado. Un inconveniente de utilizar la notación mas
popular es que la notación es mas basta que la mostrada en el ejemplo,
en el caso que la función se defina por casos se introduce nueva
sintaxis, por ejemplo la definición de una función que compute el
factorial de un número entero no negativo se escribiría:\\

$$g(n)=
\begin{cases}
  1 &\mbox{si } n = 0 \\ 
  n\times g(n-1) & \mbox{en otro caso }
\end{cases}
$$\\

Si involucramos operaciones mas complejas que la
multiplicación nos encontraremos con aún mas sintaxis, por ejemplo
$\Sigma$ para una sumatoria, $\Pi$ para productos, $\frac{df}{dx}$
para la derivadas, $\int$ para integrales e incluso la función $g$
definida previamente sería escrita como $n!$.\\

En el \calam, toda la notación se compone en definición y aplicación
de funciones, de tal manera que funciones complejas pueden ser
desmenuzadas hasta obtener únicamente una combinación de definición y
aplicación de funciones. En los ejemplos dearriba, la expresión
$\lap{\lab{x}{3\times x}}{2}$ es incorrecta, debido a que la
multiplicación debería ser una función y no un operador infijo e
incluso los números deben de ser representados con definición y
aplicación de funciones. En la parte \rom{4} de este trabajo se
abordará el problema de como representar números naturales y
operaciones aritméticas elementales.\\

En la parte \rom{2} y \rom{3} del trabajo se van a escribir
expresiones que no tienen un sentido aritmético o lógico en ellas,
serán simples funciones que manipulan otras expresiones. Ejemplos del
tipo de expresiones con las que trabajaremos en estas partes son\
\begin{align*}
  \text{a) }&x & \text{ (Una variable aislada)}\\
  \text{b) }&\lab{x}{\lap{x}{\lap{x}{y}}} & \text{ (Una función)}\\
  \text{c) }&\lap{y}{\lab{x}{\lab{x}{x}}} & \text{ (Una aplicación)}\\
  \text{d) }&\lap{\lab{y}{y}}{\lab{x}{\lap{x}{w}}} & \text{ (Una aplicación de una
                                                     función en otra)}\\
  \text{e) }&\lab{f}{\lab{x}{\lap{f}{\lap{f}{x}}}} &\text{ (Una
                                                     función con rango de
                                                     valores de funciones)}
\end{align*}

Hay algunas cuestiones peculiares de esta notación las cuales son
visibles en los ejemplos de arriba.

\subsection{Variables}
En el \calam las variables por si solas son expresiones válidas, en el
inciso a) aparece la variable $x$ la cual no es ni una definición de
función ni una aplicación de una expresión a una función.

\subsection{Aplicaciones anidadas}
Así como en la notación tradicional se pueden escribir expresiones
como $sen(ln(x))$ en el \calam también se puede anidar la aplicación
de funciones. En el inciso b) se muestra un ejemplo de esto,
$\lab{x}{\lap{x}{\lap{x}{y}}}$ consiste en aplicar $x$ al resultado de
la aplicación $\lap{x}{y}$.

\subsection{Aplicaciones sin sentido aparente}
El inciso c) muestra una expresión que no tiene sentido si
consideramos la aplicación como se abordó previamente. Sin embargo,
$\lap{y}{\lab{x}{\lab{x}{x}}}$ es una expresión válida en el \calam a
pesar de ser poco usual tratar la aplicación de una expresión en una
variable tanto en la teoría como en las aplicaciones del sistema.

\subsection{Aplicación de función a función}
Como se describió previamente, el \calam permite expresar la
aplicación de una función a otra función para definir operaciones
interesantes que manipulen expresiones (como lo son la diferenciación y
la integración). En el inciso d) se muestra como se aplica la función
identidad $\lab{y}{y}$ a la función $\lab{x}{\lap{x}{w}}$ (esta
segunda función no tiene un significado en particular).

\subsection{Funciones de varias variables}
En la notación tradicional se denotan las funciones de varias
variables como $f(x,y)=\dots$ y cuando se evalúan estas funciones se
escribe $f(v_1, v_2)$ (donde $v_1$ y $v_2$ son valores en el dominio
de $f$). En el \calam solo se pueden definir funciones en una sola
variable, sin embargo, como el sistema nos permite tener funciones con
rango de valores de funciones, se puede simular tener funciones de
varias variables. Si consideramos como ejemplo la función
$f(x,y)=sen(x)+cos(y)$ su equivalente en el \calam sería
$$\lab{x}{\lab{y}{sen(x)+cos(y)}}$$
De tal manera que si se desee evaluar $f(\pi, \frac{1}{2}\pi)$, en el
\calam se escribiría
$$\lap{\lap{\lab{x}{\lab{y}{sen(x)+cos(y)}}}{\pi}}{\frac{1}{2}\pi}$$

En el sistema, la evaluación parcial de una función como esta es muy
útil ya que permite tratar con los argumentos de la función de manera
independiente.

\section{Perspectiva operativa}

Hay una serie de operaciones elementales en el \calam que permiten la
manipulación de expresiones. La operación mas básica en el \calam y de
la cual depende el resto es la de \emph{sustitución} de una variable
en una función por alguna otra expresión del \calam. Las otras tres
operaciones tratan con el concepto de igualdad entre dos expresion y
como se puede obtener una expresión igual (o equivalente) a partir de
otra.

\subsection{Sustitución}

La operación de sustitución se encarga de cambiar la manera en como se
escribe una expresión, usualmente se sustituyen variables por
variables o variables por expresiones mas complejas. Para denotar la
sustitución de la variable $y$ por $x$ en la expresión
$\lap{y}{\lap{z}{w}}$ se escribe:
\begin{align*}
  &\lap{y}{w}[y:=x] = \lap{x}{w}
\end{align*}

En general se puede sustituír cualquier variable dentro de una
expresión por otra expresión, de tal manera que si $M$ y $N$ son
expresiones del \calam y $x$ es una variable, el sustituir a la $x$ en
$M$ por $N$ se denota $M[x:=N]$.

Otros ejemplos de sustitución son

\begin{itemize}
\item[\S] $x[x:=\lab{y}{y}] = \lab{y}{y}$
\item[\S] $\lab{f}{\lap{f}{\lap{f}{x}}}[x:=\lap{f}{x}] =
  \lab{f}{\lap{f}{\lap{f}{\lap{f}{x}}}}$
\end{itemize}

\subsection{Equivalencias}
Como se abordó previamente se pueden construír expresiones en el
\calam cuyo significado no esté definido con claridad o que el
significado de una expresión dependa del contexto en el que es
utilizada.\\

Una pregunta que se puede responder independientemente del contexto en
el que se trabaje con el sistema es, si $M$ y $N$ son dos expresiones
del \calam, ¿Son iguales?, ¿Son equivalentes?. Para responder esta
pregunta se tiene que explorar a que nos podemos referir con
\emph{igual} o \emph{equivalente}.\\

Podemos afirmar que dos cosas son \emph{lo mismo} cuando entendemos el
contexto y el nivel de detalle en el que estamos trabajando. Podemos
considerar una casa igual que otra si lo relevante es el concepto, si
consideramos el color de dos casas que tengan la misma estructura
podemos diferenciarlas por como están pintadas, sin embargo siguen
teniendo la misma función.\\

Es importante evitar la ambiguedad de el concepto de
\emph{equivalencia} y para ello abordamos algunas maneras en las
que podemos comparar dos expresiones del \calam.

\subsubsection{Equivalencia sintáctica}
La equivalencia sintáctica es aquella que nos permite distinguir si
dos expresiones son lo mismo de acuerdo a la manera en como están
escritas. Si dos expresiones $M$ y $N$ están escritas de la misma
manera, símbolo por símbolo, se dice que son equivalentes
sintácticamente (denotado como $M \eq N$.

\subsubsection{Equivalencia estructural}
Podemos ver las expresiones del \calam como aplicaciones, funciones,
variables y combinaciones de estas. La posición en donde se encuentran
las diferentes componentes de una expresión determinan su estructura,
sin embargo, los nombres de variables que se utilicen no influyen si
es que mantienen su lugar en la estructura.\\

Consideremos la función identidad $\lab{x}{x}$, esta función tiene la
misma estructura que $\lab{y}{y}$ la cual también representa la
función identidad. A pesar de no estar escritas exactamente igual, la
correspondencia que hay de la posición de la variable $x$ en la
primera expresión con la posición de la variable $y$ en la segunda
expresión y el hecho de que ambas tienen la misma estructura nos
permite decir que son lo mismo.\\

Si consideramos dos  expresiones mas complejas como
$\lab{f}{\lab{x}{\lap{f}{x}}}$ y $\lab{g}{\lab{y}{\lap{g}{y}}}$
podemos notar que también son equivalentes en este sentido. Una manera
intuitiva de pensar esta equivalencia es comprobar que ambas funciones
son leídas igual pero en lugar de pronunciar explícitamente el nombre
de la variable, mencionamos la posición de la $\lambda$ donde aparece.
En el caso de este ejemplo se leería ``Función que es evaluada a una
función que es evaluada a la aplicación del argumento de la función
mas lejana con el argumento de la función mas cercana''. Si dos
expresiones $M$ y $N$ son estructuralmente equivalentes, se
dice que $M$ es $\alpha$-equivalente a $N$ (denotado como $M \aeq N$).\\

Una notación utilizada para la corroboración de la equivalencia en este sentido
es la del \emph{índice de De Bruijn}, la cual evita la aparición de
letras en las expresiones y en su lugar utiliza números que
representan la distancia de una variable a la $\lambda$ de la función
en donde aparece como argumento. De tal manera que una expresión como
$$\lab{z}{\lap{\lab{y}{\lap{y}{\lab{x}{x}}}}{\lab{x}{\lap{z}{x}}}}$$
se escribe en la notación de De Bruijn como
$$\lab{}{\lap{\lab{}{\lap{1}{\lab{}{1}}}}{\lab{}{\lap{2}{1}}}}$$

En este trabajo no se utilizará la notación de De Bruijn, sin embargo
es importante mencionarla ya que dos expresiones que sean
$\alpha$-equivalentes van a ser sintácticamente equivalentes
utilizando los índices de De Bruijn.

\subsubsection{Equivalencia de resultados}
Otra equivalencia que podemos encontrar en las expresiones es la de
resultados, esta hace referencia a que una aplicación de una función a
una expresión es equivalente al resultado de evaluar la función con
dicha expresión como argumento. Para explicar mejor el concepto,
consideremos la función en notación tradicional $f(x)=x^2$, si se
evalúa $f(2)$ el resultado es $4$, por lo tanto es conveniente
establecer una equivalencia entre la evaluación y el resultado.\\

En el \calam se denomina a esta equivalencia como $\beta$-equivalencia
y cuando se tienen dos expresiones $M$ y $N$ que son
$\beta$-equivalentes se escribe $M \beq N$. Si tenemos por ejemplo la
función identidad $\lab{x}{x}$ podemos afirmar que para cualquier
expresión $M$
$$\lap{\lab{x}{x}}{M} \beq M$$

Es importante mencionar que si utilizamos la notación tradicional,
estas tres equivalencias se denotan con el símbolo $=$ (en los tres
casos), por ejemplo si $f(x)=x^2$ y $g(y)=y^2$, entonces $f = g$ y
$f(3)=g(3)=9$. En el \calam es importante hacer estas distinciones
porque el manejo de funciones no se aborda desde el punto de vista de
una relación entre dos conjuntos (dominio y codominio), si no como una
expresión que puede ser manipulada para convertirse en otra expresión.

\subsubsection{Equivalencia computacional}
Distinguir entre la equivalencia extensional y la intensional,
presentar el argumento algorítmico y para explicar por que es
importante distinguir un programa de otro a pesar de que computen los
mismos resultados.

$\ceq$


\subsubsection{Equivalencia de redundancia}
El último tipo de equivalencia que se abordará en este trabajo es la
de redundancia y es una equivalencia que se considera en un caso muy
específico, de hecho es un caso particular a la equivalencia
computacional.\\

Consideremos una función que esté definida a partir de la evaluación
de una función diferente a ella $f(x) = g(x)$, el papel que puede
jugar $f$ es el de ser evaluada en algún valor y resultar en la
evaluación de otra función en este valor. Sabemos que $f\not\eq g$ y
que $f\not\aeq g$ debido a que son diferentes, también sabemos que no
necesariamente se cumple que $f\beq g$ ya que $g$ pudiera ser una
función sin funciones en su rango de valores. El trabajar con $f$
resulta redundante ya que lo único que hace es evaluar otra función en
el mismo argumento, ésto nos permite afirmar que son equivalentes.\\

En el \calam, la equivalencia de redundancia se denomina
$\eta$-equivalencia (denotada con el símbolo $\eeq$) y nos permite
establecer una equivalencia entre dos expresiones que al ser aplicadas a
cualquier otra expresión $M$ el resultado es el mismo (son
$\beta$-equivalentes), es decir $$\lab{x}{\lap{G}{x}} \eeq G$$ ya
que $$\lap{\lab{x}{\lap{G}{x}}}{M} \beq \lap{G}{M}$$

\subsection{Conversión y reducción}

\subsubsection{Cambio de nombre en argumentos de función}
Convertir una expresión $M$ a otra expresión $N$ tal que $M\aeq N$ se
denota como $M \aco N$, esta operación es llamada \acon.

\subsubsection{Reducción de aplicaciones}
Transformar la aplicación de una función $F$ a una expresión cualquiera
$M$ en el resultado $N$ de evaluar la función en el argumento se
denota $\lap{F}{M}\bre N$, esta operación es llamada \bred.

\subsubsection{Reducción de redundancia}
Transformar una definición de función redundante de la forma
$\lab{x}{\lap{M}{x}}$ en la expresión $M$ es denotado como
$\lab{x}{\lap{M}{x}} \ere M$, esta operación es llamada \ered.

\subsection{Detalles de la sustitución}
Explicar por qué se debe tener cuidado con la operación de sustitución.